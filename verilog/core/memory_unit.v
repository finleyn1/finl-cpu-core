/*****
Memory unit for Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/


module memory_unit(
	input [15:0] C_read, 
	input [10:0] addr_in,
	input imm_op,
	input [10:0] imm_data,
	output [10:0] data_addr,
	output [15:0] write_data
);
	
	wire [10:0] offset_addr;
	
	assign offset_addr = addr_in + { imm_data[3]?12'hFFF:12'd0 ,imm_data[3:0] };
	
	assign data_addr = imm_op ? imm_data : offset_addr;
	assign write_data = C_read;

endmodule

