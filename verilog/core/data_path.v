/*****
Data Path for Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/




//data movement and ALU/shifter operations
module data_path(
	//data input
	input [15:0] A, B, //A and B ports from register file
	input [15:0] CR_in, //Selected CR read data 
	input [7:0] imm_data, //imm data from decoder
	
	//control lines
	input imm_sel, ALU_op, CR_op,
	input CR_store,
	input math_op, shift_op, move_op,
	input negate,
	input [1:0] ALU_opcode,
	input [3:0] move_sz,
	input shift_dir,
	
	//Status register lines
	input C_in, //carry in
	output [3:0] SR_status_out,
	
	//data path output
	output reg [15:0] D
);
	//shifter control lines
	wire field_fill, field_mask, mask_MSB;
	
	reg [15:0] move_mask;//output of move unit
	
	//ALU input and output
	wire [15:0] ALU_A, ALU_out;
	
	reg [15:0] ALU_B; //ALU and mov operaand B

	wire [15:0] shift_out;

	assign ALU_A = A; //ALU operand A always read port A



	//set second operand based on control signals for ALU and mov operations
	always @(imm_sel,imm_data,CR_in, CR_op, CR_store,B)
	begin
		if(imm_sel)  //if is immediate, use immediate data
			ALU_B = {8'd0,imm_data} ;
		else if(CR_op) //if a control register
			ALU_B = CR_store ? B : CR_in; //write or read from control re
		else
			ALU_B = B; //otherwise use register file port B
	end


	//choose where output of data path comes from
	always @(move_op, negate, move_mask, ALU_out,ALU_op,shift_op,shift_out)
	begin
		if(move_op) //if a mov, from the move output
			D = negate? ~move_mask : move_mask;
		else if(ALU_op) //from ALU
			D = ALU_out;
		else if(shift_op) //from Shifter
			D = shift_out;
		else //otherwise ALU not being used
			D = 16'd0;
	end

	//ALU
	alu alunit(
		.A(A),
		.B(ALU_B),
		.op({math_op, ALU_opcode}),
		.C_in(C_in),
		.F(ALU_out),
		.N_out(SR_status_out[3]),
		.Z_out(SR_status_out[2]),
		.C_out(SR_status_out[1]),
		.V_out(SR_status_out[0])
	);

	//Barrel Shifter
	shifter barrel(
		.X(A),
		.N(imm_data[3:0]),
		.shift_left(shift_dir),
		.fieldmask(field_mask),
		.field_fill(field_fill),
		.mask_MSB(mask_MSB),
		.Y(shift_out)
	);	

	
	//mask rotate bits, for actual shifting
	assign field_mask = (shift_op & ~ALU_opcode[0]);

	//if shifting right, vacate the MSb,
	//if shifting left vacate the LSb
	assign mask_MSB = ~shift_dir;

	//use fill for ASR when right shift, and negative 
	//otherwise vacant bit is 0
	assign field_fill = (shift_op & ~shift_dir & ~(|ALU_opcode) & A[15]);


	//calculate move nibble mask
	always @(ALU_B, move_sz) //mask second operand based on bits in opcode
	begin
		move_mask = { {4{move_sz[3]}}, {4{move_sz[2]}}, {4{move_sz[1]}}, {4{move_sz[0]}} } &ALU_B;
	end


endmodule
