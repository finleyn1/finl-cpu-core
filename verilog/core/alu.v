/*****
ALU for Simple Single cycle RISC CPU with debug interface.

Copyright 2019, Nathan Finley.
All rights reserved.

	ALU opcode: 
	if op[2]==1 operation uses adder, op[1]==1 is sub, op[1]==1 is add, op[0] is carrybit
	if op[2]==0 logic operation, op[1:0] select logic operation
****/
module alu(
	input [15:0] A, B, //input operands
	input [2:0] op, //alu opcode
	input C_in, //carry in from CR

	output reg [15:0] F, //output of ALU
	output N_out,Z_out,C_out,V_out //status out of ALU
);

	//adder lines
	wire [15:0] add_out;
	wire add_C;
	wire add_V;
	wire add;
	wire carry;

	adder addsub(
		.A(A),
		.B(B),
		.C_in(carry),
		.add(add),
		.F(add_out),
		.C_out(add_C),
		.V_out(add_V)
	);

	
	assign N_out = F[15];	//use sign bit to determine if negative
	assign Z_out = ~(|F);	//reduction nor (all bits clear)


	assign add = ~op[1];
	
	assign carry = 
		(~add & ~op[0] ) |		//normal subtraction
		(add & op[0] & C_in) |		//addition with carry
		(~add & op[0] & ~C_in) | 	//subtract with carry
		(~op[2]);			//normal subtraction for compares

	//carry and overflow only relevant when add operation
	assign C_out = add_C & op[2];
	assign V_out = add_V & op[2];

	always @(C_in,A,B,op,add_out,add_C,add_V)
	begin
		if(op[2]) //output from adder
			F=add_out;
		else
			case(op[1:0]) //output from logic functions
				2'b00: F= A&B;		//and
				2'b01: F = A|B;		//or
				2'b10: F= A^B;		//xor
				2'b11: F=add_out;	//cmp
			endcase
	end
endmodule





module adder(
	input [15:0] A,B, //adder input
	input C_in, //carry in
	input add, //add==1 add operation, add==0 sub operation

	output [15:0] F, //result
	output C_out, //carry out
	output V_out //overflow out
);
	wire [16:0] add_out; //17-bit result


	//complement B if in subtract mode
	assign add_out = (A + (B^{16{~add}}) ) + C_in;
	assign C_out = add_out[16];
	assign F = add_out[15:0];

	//overflow output based on signs
	assign V_out = 
	(~F[15] & A[15] & ~B[15] &~add)|	//positive result, -A sub B
	(F[15] & ~A[15] & B[15] &~add)|		//negative result, A sub -B
	(F[15] & ~A[15] & ~B[15] &add)|		//negative result, A + B
	(~F[15] & A[15] & B[15] &add) ;		//postive result, -A + -B


endmodule
