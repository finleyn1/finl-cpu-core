/*****
Instruction Decoder for Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/

module instruction_decoder(
	input [15:0] opcode,		//opcode from instruction register

	//operation selection
	output reg alu_op, math_op, shift_op, move_op,
	output reg interrupt_return,
	output reg [1:0] alu_opcode,	//secondary ALU opcode
	output reg shift_dir,
	output reg negate,		//negates integer unit output
	output reg [3:0] move_sz,
	output reg imm_op,	//whether operation uses immediates
	output reg mem_access, mem_write,

	output reg cr_op,
	output reg cr_store, 
	output reg SP_inc, SP_dec,	//SP_control lines
	output reg write_enable,		//master enable for register file write
	output reg SR_write_en, XSR_write_en, XIP_write_en,

	output reg branch, link,

	output reg [2:0] condition_code,
	
	output reg [1:0] cr_sel,		//control register select
	output [2:0] A_reg_sel,
	output reg [2:0] B_reg_sel, C_reg_sel, D_reg_sel,		
	output reg [10:0] imm_data		//11-bit immediate (usually 8-bit unsigned)
    );


	assign A_reg_sel = opcode[10:8];
	//assign B_reg_sel = opcode[7:5];


	always @(opcode)
	begin

		//default most lines low
		alu_op=0;math_op=0;shift_op=0;move_op=0;mem_access=0;
		interrupt_return=0;
		write_enable=0;
		alu_opcode=2'd0;
		negate=0;
		SP_dec=0;
		SP_inc=0;	
		cr_op=0;
		cr_store=0;
		cr_sel=0;
		SR_write_en=0;
		XSR_write_en=0;
		XIP_write_en=0;
		link=0;
		imm_op=0;
		shift_dir=0;
		move_sz= 4'hF;	//move word by default
		imm_data=11'd0;
		mem_write=0;
		D_reg_sel=0;
		B_reg_sel=0;
		branch=0;
		condition_code=0;


			B_reg_sel=opcode[7:5];
			C_reg_sel=opcode[10:8];
			D_reg_sel=opcode[10:8];	//almost always write to operand A

		
			if(opcode[15])	//ALU operation
			begin
				SR_write_en=1;	//allow status register updates
				XSR_write_en=1;	
				
				write_enable=1;
				alu_op=1;
				if(opcode[14])	//add/subtract
				begin
					math_op=1;
					alu_opcode = {opcode[13],opcode[11]};	//carry/subtract
				
					imm_op=opcode[12];
				end
				else	//logic operation
				begin
					alu_opcode = opcode[12:11];	//logic opcode
					
					if(&opcode[12:11])	//no writeback for compare
						write_enable=0;

					imm_op=opcode[13];
				end

				imm_data = {3'd0,opcode[7:0]};
			end
			else if(opcode[15:14]==2'b01)	//shifter or move operation
			begin
				write_enable=1;	//always write when moving or shifting

				if(opcode[13:12]==2'b11)	//shifter operation
				begin
					shift_op=1;	

					imm_op=1;	//always use immediate for shift amt
					shift_dir=opcode[11];	//direction bit
					alu_opcode = opcode[7:6]; //operation select
					imm_data = {7'd0,opcode[3:0]};	//shift amount
				end
				else	//if not shift movement operation
				begin
					move_op=1;
					if(opcode[13])	//normal move
					begin 
						if(opcode[11])	//move immediate to GPR
						begin
							imm_op=1;
							imm_data = {3'd0,opcode[7:0]};
						end
						else	//move GPR to GPR
						begin
							move_sz = opcode[3:0]; 
							negate = opcode[4];
						end
					end
					else	//control register move
					begin
						cr_op=1;
					
						cr_sel=opcode[7:6];			
		
						if(opcode[12])	//control register move to GPR	
						begin
							move_sz = opcode[3:0]; 
							negate = opcode[4]; 
						end
						else	//move to control register
						begin
							write_enable=0;
							cr_store=1;

							if(opcode[7])	//if shadow
							begin
								if(opcode[6])	//ifSR
									XSR_write_en=1;
								else
									XIP_write_en=1;
							end
							else
								if(opcode[6])//if SR
									SR_write_en=1;

							//don't use to change IP

							if(opcode[11])	//immediate store to CR
							begin
								imm_op=1;
								imm_data={5'd0,opcode[5:0]};
							end
							else
							begin
								//otherwise move like other
								move_sz = opcode[3:0]; 
								move_sz = opcode[3:0]; 
								negate = opcode[4];
								B_reg_sel=opcode[10:8];
							end
						end		
					end
						
				end
			end
			else if(opcode[15:13]==3'b001)	//branching operation
			begin
				branch=1;
				if(~opcode[12] & ~opcode[7])	//return from interrupt
				begin
					SR_write_en = 1;	//update SR(from XSR)
					interrupt_return=1;	//signal return
				end
				else	//normal branch
				begin
					condition_code = opcode[2:0];	
					link=opcode[11];	//link bit

					if(opcode[12])	//relative branch
					begin
						imm_op=1;
						imm_data={3'd0,opcode[10:3]};
					end
				end
			end
			else if(opcode[15:13]==3'b000)	//memory access
			begin
				mem_access=1;
				mem_write = opcode[12];		//store bit
				write_enable=~opcode[12];	//otherwise load to register
				
				if(opcode[11])	//absolute addressing
				begin
					C_reg_sel = 5;	
					D_reg_sel = 5;	//use the MBR for absolte addressing
					imm_op=1;	
					imm_data=opcode[10:0];
				end
				else
				begin
					if(opcode[4])		//stack operation
					begin
						B_reg_sel=6;	//use SP as address

						imm_data = 11'd0;

						if(opcode[3]) //if updating SP after access
							if(opcode[2])		//incremnt
							begin
								SP_inc=1;

								if(~opcode[1])	//if 'update before' offset address by 1
									imm_data = {7'd0,4'd1};
							end
							else			//decrement
							begin
								SP_dec=1;

								if(~opcode[1]) //if 'update before' offset address by -1
									imm_data = {7'd0,4'hF};
							end
					
					end
					else	//non-stack, indirect addressing
					begin
						imm_data={7'd0,opcode[3:0]};
					end
				end			

			end
	end


endmodule


