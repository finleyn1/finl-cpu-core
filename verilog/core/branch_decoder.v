/*****
Branch Decoder Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/


module branch_decoder(
	input [2:0] cond_code,
	input N_status,
	input Z_status,
	input [15:0] ip_addr,
	input [7:0] offset,
	input branch,
	output reg cond_true,
	output [15:0] calc_addr
);
	
	//use condition code from opcode and status bits to
	//determine if branch is take
	always @ (cond_code, N_status, Z_status,branch)
	begin
		if(~branch)
			cond_true =0;
		else
			case(cond_code)
				3'b000 :cond_true = 1;				//always
				3'b001 :cond_true = ~N_status & ~Z_status;	//greater
				3'b010 :cond_true = N_status;			//less than
				3'b011 :cond_true = Z_status;			//equal/zero
				3'b100 :cond_true = ~Z_status;			//neq/not zero
				3'b101 :cond_true = N_status;			//negative
				3'b110 :cond_true = ~N_status;			//positive
				3'b111 :cond_true = 0;				//never
			endcase
	end

	//calculate relative address from FIP value and immediate
	//sign extend if negative
	assign calc_addr = (ip_addr + ((offset[7])? {8'hFF,offset}:{8'd0,offset}));


endmodule


