/*****
Barrel Shifter for Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/


//16-bit shifter wrapper
//uses masking unit
module shifter(
	input [15:0] X,
	input [3:0] N,
	input shift_left,
	input fieldmask,field_fill,mask_MSB,
	output [15:0] Y
);
	
	wire [15:0] preout,postin,shiftout;


	//reorder bits if shifting right (barrel only shifts left)
	reorder #(16) preorder(
		.en(~shift_left),	
		.X(X),
		.Y(preout)
	);


	//actual barrel shifter
	barrel #(16) shift(
		.X(preout),
		.N(N),
		.Y(postin)
	);

	//rereorder bits if shifting right
	reorder #(16) postorder(
		.en(~shift_left),
		.X(postin),
		.Y(shiftout)
	);


	//mask for shifts (not rotates)
	//fill 1's if necessary for arithmetic shifts
	maskunit #(16) fieldsel(
		.A(shiftout),
		.en(fieldmask),
		.dir(mask_MSB),	
		.F(field_fill),
		.N(N),
		.Y(Y)
	);



endmodule


//reverses order of bits in input to output
module reorder #(WIDTH=8)
(
    input en,
    input [WIDTH-1:0] X,
    output [WIDTH-1:0] Y
);

    genvar i;
    generate
        for(i=0;i<WIDTH;i=i+1)
        begin
            assign Y[i] = en?X[i]:X[WIDTH-1-i];
        end
    endgenerate

endmodule

module barrel #(WIDTH=8)    //the actual rotate mechanism
(
input [WIDTH-1:0] X,        //WIDTH bits wide
input [$clog2(WIDTH)-1:0] N,    //NEED log2(WIDTH) bits to control it
output [WIDTH-1:0] Y
 );


	wire [WIDTH**2-1:0] mat;       //matrix for wiring conections

	genvar i;
	genvar j;

	generate       //calulate inputs muxes
	for(i=0;i<WIDTH;i=i+1) 
	begin
		for(j=0;j<WIDTH-i;j=j+1) 
		begin
			assign	mat[WIDTH*i+j] = X[j+i]; 
		end
		for(j=WIDTH-i;j<WIDTH;j=j+1) 
		begin
			assign mat[i*WIDTH+j] = X[j-(WIDTH-i)]; 
		end
	end
	endgenerate


	generate       //generate the muxes based on the matrix we calculated
	for(i=0;i<WIDTH;i=i+1)
	begin: muxes 
    if(i!=0)
	barrelmux #(.WIDTH(WIDTH)) a (
		.X(mat[i*WIDTH+(WIDTH-1):i*WIDTH]),
		.sel(N),
		.Y(Y[i-1])
	);
	else
	barrelmux #(.WIDTH(WIDTH)) a (
            .X(mat[i*WIDTH+(WIDTH-1):i*WIDTH]),
            .sel(N),
            .Y(Y[WIDTH-1])
        );
	end
	endgenerate


endmodule


//Muxes used for the shifter
module barrelmux  //WIDTH bit wide mux
#(WIDTH=8)
(
input [WIDTH-1:0] X,
input [$clog2(WIDTH)-1:0] sel,
output reg Y
);

	always @(sel,X)
		Y = X[sel];


endmodule



module maskunit#(WIDTH=16)(
input [WIDTH-1:0]A,
input en,
input [$clog2(WIDTH)-1:0] N,
input dir,
input F,
output[WIDTH-1:0] Y
    );
    
    wire [WIDTH-1:0] decout;
    
    maskdecoder #(WIDTH) dec(
    .N(N),
    .dir(dir),
    .Y(decout)
    );

	//mask the bits with 1's if the F bit is set
	//and mask out the bits of A if enabled
    assign Y = en?((decout&{16{F}})|(~decout&A)):A;
    
endmodule


module maskdecoder #(WIDTH=16)(
input [$clog2(WIDTH)-1:0] N,
input dir,
output  [WIDTH-1:0] Y
);
	genvar i;
	
	//for each bit, if its above/below threshold (based on direction)
	//output a 1 if the bit is masked, used above for 'fill'
	generate 
		for(i=0;i<WIDTH;i=i+1)
			assign Y[i] = (dir?(N>=WIDTH-1-i):(N>=i));
	endgenerate

endmodule
