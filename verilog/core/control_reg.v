/*****
Control Registers for Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/


module instr_ptr(
	input [15:0] branch_addr, xip_input,
	input clk, rst, IRQ, intr_en, branch, rti, xip_write_en, ce,
	input [15:0] ip_input,
	input debug_en,ip_write_en,
	output reg [15:0] fip, xip
);



	wire [15:0] fip_in, xip_in;

	//new address if branch, otherwise increment
	assign fip_in = branch ? branch_addr : fip+16'd1;
	
	assign xip_in = fip;

	

	//fetch pointer logic
	always @(posedge clk)
	begin
		if(rst)
			fip<=16'd0;
		else if(ip_write_en&debug_en) //debug write
			fip<=ip_input;	
		else if(ce&~debug_en) //normal operation
		begin
			if(IRQ & intr_en)	//accepted interrupt
				fip<=16'd1; //jump to address 1
			else if(rti & ~intr_en) // return from interrupt (interrupts must be disabled)
				fip<=xip; //writes shadow to the pointer
			else
				fip<=fip_in;
		end
	end
	
	//shadow ip logic
	always @(posedge clk)
	begin
		if(rst)
			xip<=0;
		else if(xip_write_en&debug_en) //debug write
			xip<=xip_input;
		else if(ce&~debug_en)
		begin
			if(intr_en)	//shadow if intr enabled
				xip<=fip_in;
			else if(xip_write_en &~intr_en) //only write if intr disbl
				xip<=xip_input;
		end
	end

endmodule




module status_reg(
	input [4:0] SR_in,
	input [3:0] XSR_in,
	input SR_write_en, XSR_write_en,
	input clk, rst, IRQ, rti,ce,
	output [15:0] SR_out, XSR_out,
	input debug_en
);
	
	reg [3:0] SR, XSR; //actual registers
	reg intr_en; //interrupt bit is it's own thing

	//SR is only 5-bits wide, read rest as zero
	//interrupt bit reflects same in both SR and XSR
	//can only change interrupt bit in SR, however
	assign SR_out = {9'd0,intr_en,SR};
	assign XSR_out = {9'd0,intr_en,XSR};
	
	always @(posedge clk)
	begin
		if(rst)
		begin
			SR<=0;
			XSR<=0;
			intr_en<=0;
		end
		else if(debug_en) //debug operations
		begin
			if(SR_write_en) //debug write to sr
			begin
				SR<=SR_in[3:0];
				intr_en<=SR_in[4];
			end
			
			if(XSR_write_en) //debug write o xsr
				XSR<=XSR_in;
		end
		else if(ce&~debug_en) //normal operation
		begin
			if(~rti) //if not return from interrupt
			begin
				if(IRQ & intr_en) // if irq enabled and interrupt request
					intr_en<=0; //disable interrupts (and register shadows
				else if(SR_write_en) //
					intr_en<=SR_in[4];

				if(SR_write_en)
				begin
					if(intr_en | SR_in[4]) //if interrupts enabled, or being enabled, save to XSR
						XSR <= SR_in[3:0];
					
					SR<= SR_in[3:0]; //write to SR
				end
				else if(XSR_write_en) //writing to XSR alone
				begin
					if(~intr_en) //interrupts must be disabled
						XSR<=XSR_in[3:0];
				end
			end
			else if(rti) //return from interrupt
			begin
				if(~intr_en)// only rti when disabled
				begin
					SR <=XSR; //restore SR from shadow
					intr_en <=1; //reenable interrupts
				end
			end
		end
	end

endmodule
