/*****
Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/



//debug version of CPU core (has register read ports)
module finl_core_debug(
	input clk, rst, IRQ, 
	input ce, //clock enable
	input [15:0] prog_mem_in, data_mem_in, //data in from memories
	output [10:0] prog_mem_addr, //program address out
	output [10:0] data_mem_addr, //data mem address out
	output [15:0] data_mem_out, //data mem write data out
	output data_mem_write_en, //indicate write to memory

	input debug_en,	//debug enable signla
	input [3:0] E_sel, //debug read select
	input E_write_en, //debug write enable
	input [15:0]  E_data_in, //debug write data
	output reg [15:0] E_read //debug read data
);



	//output from data path to register file
	wire [15:0] dp_out;

	//data ports from control registers
	wire [15:0] FIP_read, XIP_read,SR_read,XSR_read;

	//3 read ports from register file
	wire [15:0] A_read,B_read,C_read;
	//write port to register file
	wire [15:0] D_write;
	
	//opcode from memory
	wire [15:0] opcode;

	//write ports for control registers
	wire [4:0] SR_write;
	wire [3:0] XSR_write;
	wire [15:0] XIP_write;


	//status from ALU to status register
	wire [3:0] ALU_status_out;
	
	
	//output of CR read decoder
	reg [15:0] CR_read;

	//branch address from the branch decoder or register file
	wire [15:0] rel_branch_addr, branch_addr;


	//control signals to/from instruction decoder

	//register file control signals
	wire [2:0] A_sel, B_sel, C_sel, D_sel_in; //register file port select
	wire [10:0] imm_data; //immediate data decoded from opcode
	wire write_en; //enable write to register file
	wire SP_in,SP_dec; //if SP is incremented or decremented by operation
	wire link; //if branch writes to the link register

	//interrupt control signals
	wire intr_en; //indicating interrupts are enabled
	wire rti; //indicating instruction is return from interrupt
	
	//data path control signals
	wire alu_op, math_op, shift_op, move_op; //indicates type of data operation
	wire [1:0] alu_opcode; //auxillary alu opcode
	wire shift_dir; //shift direction
	wire negate; //if a move is a negated move
	wire [3:0] move_sz; //which nibbles of the word to mask
	wire [1:0] CR_sel; //which CR to read from
	wire CR_op; //if the operation is to or from a cr
	wire CR_store; //if the operation writes to a Cr
	wire imm_op; //if the operation uses immediate data
	wire mem_access; //if the operation accesses memory
	wire mem_write; //if the operation writes to memory
	//write enable signals for the control registers
	wire SR_write_in, XSR_write_in, XIP_write_in;


	//branch control signals
	wire branch; //if operation is a branch
	wire br_take; //if the branch is taken 
	wire [2:0] condition_code; //condition code sent to branch decoder




	//interrupt enable is status register bit 4
	assign intr_en = SR_read[4];

	//write status bits to the SR if an alu operation is performed, otherwise write data
	assign SR_write = (alu_op) ? {SR_read[4],ALU_status_out} : D_write[4:0];
	//XSR has same data input
	assign XSR_write = SR_write[3:0];
	
	assign XIP_write = D_write;
	
	//use a relative address, or get address from register file
	assign branch_addr = (imm_op)?rel_branch_addr: A_read;

	//choose source of data to register file's write port
	assign D_write = mem_access ? data_mem_in : dp_out;

	//output the instruction pointer's address to the program memory
	assign prog_mem_addr = FIP_read;

	//opcode from program memory input
	assign opcode = prog_mem_in;
	
	
	assign data_mem_write_en =  mem_write;

	//decode reading from control registers
	always @(CR_sel, FIP_read, XIP_read, SR_read, XSR_read)
	begin
		case(CR_sel)
			2'b00: CR_read = FIP_read;
			2'b01: CR_read = SR_read;
			2'b10: CR_read = XIP_read;
			2'b11: CR_read = XSR_read;
		endcase
	end







	//instruction decoder, drives control signals
	instruction_decoder instr_dec(
		.opcode(opcode),
		.alu_op(alu_op),
		.math_op(math_op),
		.shift_op(shift_op),
		.move_op(move_op),
		.cr_op(CR_op),
		.cr_store(CR_store),
		.interrupt_return(rti),
		.alu_opcode(alu_opcode),
		.shift_dir(shift_dir),
		.negate(negate),
		.move_sz(move_sz),
		.imm_op(imm_op),
		.mem_access(mem_access),
		.mem_write(mem_write),	
		.write_enable(write_en),
		.SP_inc(SP_inc),
		.SP_dec(SP_dec),	
		.cr_sel(CR_sel),
		.SR_write_en(SR_write_in),
		.XSR_write_en(XSR_write_in),
		.XIP_write_en(XIP_write_in),
		.branch(branch),
		.condition_code(condition_code),
		.link(link),
		.A_reg_sel(A_sel),
		.B_reg_sel(B_sel),
		.C_reg_sel(C_sel),
		.D_reg_sel(D_sel_in),
		.imm_data(imm_data)
	);

	//decodes branches, calculates relative addresses
	branch_decoder branch_decode(
		.cond_code(condition_code),
		.N_status(SR_read[3]),
		.Z_status(SR_read[2]),
		.ip_addr(FIP_read),
		.offset(imm_data[7:0]),
		.branch(branch),
		.cond_true(br_take),
		.calc_addr(rel_branch_addr)
	);



	//register File with debug read and write support
	GPR_file_debug rfile(
		.A_sel(A_sel),
		.B_sel(B_sel),
		.C_sel(C_sel),
		.D_sel(D_sel_out),
		.D(D_write_data),
		.E_sel(E_sel[2:0]),
		.E(E_reg_read),
		.debug_en(debug_en),
		.link_addr(FIP_read),
		.write_en(D_write_en),
		.clk(clk),
		.rst(rst),
		.ce(ce),
		.link(link),
		.SP_inc(SP_inc),
		.SP_dec(SP_dec),
		.A(A_read),
		.B(B_read),
		.C(C_read)
	);
	
	//instruction pointer
	instr_ptr IP(
		.clk(clk),
		.rst(rst),
		.ce(ce),
		.IRQ(IRQ),
		.branch_addr(branch_addr),
		.xip_input(XIP_write_data),
		.intr_en(intr_en),
		.branch(br_take),
		.rti(rti),
		.xip_write_en(XIP_write_out),
		.fip(FIP_read),
		.xip(XIP_read),
		.debug_en(debug_en),
		.ip_write_en(IP_write_out),
		.ip_input(IP_write_data)
	);
	
	//status register and shadow
	status_reg SR_reg(
		.SR_in(SR_write_data),
		.XSR_in(XSR_write_data),
		.SR_write_en(SR_write_out),
		.XSR_write_en(XSR_write_out),
		.clk(clk),
		.rst(rst),
		.ce(ce),
		.IRQ(IRQ),
		.rti(rti),
		.SR_out(SR_read),
		.XSR_out(XSR_read),
		.debug_en(debug_en)
	);


	//data_path for alu/shift/mov operations
	data_path dp(
		.A(A_read),
		.B(B_read),
		.CR_in(CR_read),
		.imm_data(imm_data[7:0]),
		.imm_sel(imm_op),
		.ALU_op(alu_op),
		.CR_op(CR_op),
		.CR_store(CR_store),
		.math_op(math_op),
		.shift_op(shift_op),
		.move_op(move_op),
		.negate(negate),
		.ALU_opcode(alu_opcode),
		.move_sz(move_sz),
		.shift_dir(shift_dir),
		.C_in(SR_read[1]),	
		.SR_status_out(ALU_status_out),
		.D(dp_out)	

	);

	//controls memory address output
	memory_unit memory(
		.C_read(C_read),
		.addr_in(B_read[10:0]),
		.imm_op(imm_op),
		.imm_data(imm_data),
		.data_addr(data_mem_addr[10:0]),
		.write_data(data_mem_out)
	);
	
	
	
	//debug suppport wires
	wire [15:0] D_write_data; //output from debug unit to reg file
	wire D_write_en; //enables write to register file
	//output from debug unit to control registers
	wire [15:0] IP_write_data, XIP_write_data; 
	wire [4:0] SR_write_data;
	wire [3:0] XSR_write_data;
	//otuput from debug unit to control register write enables
	wire IP_write_out, SR_write_out, XIP_write_out, XSR_write_out;
	
	//write port select to register file
	wire [2:0] D_sel_out;
	
	//port E for reading from register file
	wire [15:0] E_reg_read;
	
	//debug port E read muxing
	always @(E_sel,E_reg_read,FIP_read,SR_read,XIP_read,XSR_read)
	begin
		if(E_sel[3])
			E_read = E_reg_read;
		else if(~E_sel[3] & E_sel[2])
			E_read = 16'hDEAD;
		else
			case(E_sel[1:0])
				0: E_read = FIP_read;	
				1: E_read = SR_read;	
				2: E_read = XIP_read;	
				3: E_read = XSR_read;	
			endcase
	end


	//decoding debug bus signals
	debug_decode dbdec(
		.E_sel(E_sel),
		.E_write_en(E_write_en),
		.E_data_in(E_data_in),
		.debug_en(debug_en),
		.D_write_data(D_write),
		.D_write_in(write_en),
		.D_write_out(D_write_data),
		.D_write_en(D_write_en),
		.D_sel_in(D_sel_in),
		.D_sel_out(D_sel_out),
		.SR_write_in(SR_write_in),
		.XSR_write_in(XSR_write_in),
		.XIP_write_in(XIP_write_in),
		.IP_write_out(IP_write_out),
		.XIP_write_out(XIP_write_out),
		.SR_write_out(SR_write_out),
		.XSR_write_out(XSR_write_out),
		.SR_in(SR_write),
		.XSR_in(XSR_write),
		.XIP_in(XIP_write),
		.IP_out(IP_write_data),
		.SR_out(SR_write_data),
		.XIP_out(XIP_write_data),
		.XSR_out(XSR_write_data)

	);


endmodule


//decode debug write ports
//
module debug_decode(
	input [3:0] E_sel,
	input E_write_en,
	input [15:0] E_data_in,
	input debug_en,
	input [15:0] D_write_data,
	input D_write_in,
	output [15:0] D_write_out,
	output D_write_en,
	input [2:0] D_sel_in,
	output [2:0] D_sel_out,
	input SR_write_in,XSR_write_in,XIP_write_in,
	output IP_write_out,XIP_write_out,SR_write_out,XSR_write_out,
	input [4:0] SR_in,
	input [3:0] XSR_in,
	input [15:0] XIP_in,
	output [15:0] IP_out,XIP_out,
	output [4:0] SR_out,
	output [3:0] XSR_out
);

	//only the debug module can "write" to the FIP
	assign IP_write_out = debug_en ? E_write_en & (E_sel==4'b0000) : 1'b0;
	
	//other control registers, if not debug write, pass the core's write enables
	assign SR_write_out = debug_en ? E_write_en & (E_sel==4'b0001) : SR_write_in;
	assign XIP_write_out = debug_en ? E_write_en & (E_sel==4'b0010) : XIP_write_in;
	assign XSR_write_out = debug_en ? E_write_en & (E_sel==4'b0011) : XSR_write_in;


	//mux write data fro control registers
	assign IP_out = debug_en ?  E_data_in : 16'd0;
	assign SR_out = debug_en ? E_data_in[4:0] : SR_in;
	assign XIP_out = debug_en ?  E_data_in : XIP_in;
	assign XSR_out = debug_en ?  E_data_in[3:0] : XSR_in;

	//use the GPR File's write port for debug write
	assign D_write_en = debug_en ? (E_write_en & E_sel[3]) : D_write_in;
	assign D_sel_out = debug_en ? E_sel[2:0] : D_sel_in;
	assign D_write_out = debug_en ? E_data_in : D_write_data;

	

endmodule


