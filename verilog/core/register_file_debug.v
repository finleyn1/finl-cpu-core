/*****
Register File Simple Single cycle RISC CPU with debug interface.


Copyright 2019, Nathan Finley.
All rights reserved.
*****/



//debug version of register file (has extra read port)
module GPR_file_debug(
	input [2:0] A_sel, B_sel, C_sel, D_sel,
	input [15:0] D,
	input [15:0] link_addr,
	input write_en, clk, rst, link, ce,
	input SP_inc, SP_dec,
	output [15:0] A,B,C,

	input debug_en,
	input [2:0] E_sel,
	output [15:0] E
);

	//output of write decoder to registers
	wire [7:0] dec_sel;
	
	//data out from first 5 registers
	wire [16*8-1:0] GPR_data;
	//data into and out of link regsiter
	wire [15:0] LR_data_in, LR_data_out;
	//data in and out of stack pointer
	wire [15:0] SP_data_out;
	reg [15:0] SP_data_in;

	//write eanbles for stack poitner and link reg
	wire SP_write_en, LR_write_enable;
	


	//write enable 3 to 8  decoder
	decoder38 write_decode(
		.I(D_sel),
		.en(1'b1),
		.Y(dec_sel)
	);


	//muxes for 3 read ports
	mux168 A_mux(
		.I(GPR_data),	
		.sel(A_sel),
		.Y(A)
	);

	mux168 B_mux(
		.I(GPR_data),	
		.sel(B_sel),
		.Y(B)
	);

	mux168 C_mux(
		.I(GPR_data),	
		.sel(C_sel),
		.Y(C)
	);


	//debug  read port mux
	mux168 E_mux(
		.I(GPR_data),	
		.sel(E_sel),
		.Y(E)
	);

	//generate first 6 registers
	genvar i;
	generate
		for(i=0;i<6;i=i+1)
		begin: GPRs
			GPR a(
				.D(D), //all input from the data path
				.write_en(write_en & dec_sel[i]), //use appropriate bit from decoder
				.rst(rst),
				.clk(clk),
				.ce(ce),
				.debug_en(debug_en),
				.Q(GPR_data[(i*16)+:16]) //data out to register port muxes
			);
		end
	endgenerate

	//stack pointer
	GPR SP(
		.D(SP_data_in),
		.write_en(SP_write_en),
		.rst(rst),
		.clk(clk),
		.ce(ce),
		.debug_en(debug_en),
		.Q(SP_data_out)
	);

	//link register
	GPR LR(
		.D(LR_data_in),
		.write_en(LR_write_en),
		.rst(rst),
		.clk(clk),
		.ce(ce),
		.debug_en(debug_en),
		.Q(LR_data_out)
	);

	//output of SP and LR to port mux
	assign GPR_data[6*16 +:16] = SP_data_out;
	assign GPR_data[7*16 +:16] = LR_data_out;

	//if branch and link, put return address in LR, else data from normal data path
	assign LR_data_in = (~debug_en&link)? link_addr+1 : D;
	assign LR_write_en = (~debug_en&link) | (write_en & dec_sel[7]);


	//write to SP if doing stack operation, or a standared write to r6
	assign SP_write_en = (~debug_en&(SP_inc | SP_dec)) | (write_en & dec_sel[6]);
	
	//SP inc/dec logic
	always @(SP_inc,SP_dec,SP_data_out,D,debug_en)
	begin
		if(SP_inc&~debug_en)
			SP_data_in = SP_data_out +16'd1;
		else if(SP_dec&~debug_en)
			SP_data_in = SP_data_out -16'd1;
		else
			SP_data_in = D;
	end


endmodule


module GPR(
	input [15:0] D, //data in
	input clk, write_en,rst,ce,
	input debug_en, 
	output reg[15:0] Q
);
	always @(posedge clk)
	begin
		if(rst)
			Q<=16'd0;
		else if(debug_en&write_en) //debug write
			Q<=D;
		else if(write_en&ce&~debug_en) //otherwise use the CPU clock enable
			Q<=D;
	end

endmodule

//3 to 8 binary decoder
module decoder38(
	input [2:0] I,
	input en,
	output reg [7:0] Y
);
	always @(I,en)
	begin
		if(en)
			case(I)
				0: Y=8'd1;
				1: Y=8'd2;
				2: Y=8'd4;
				3: Y=8'd8;
				4: Y=8'd16;
				5: Y=8'd32;
				6: Y=8'd64;
				7: Y=8'd128;
			endcase
		else
			Y =8'd0;
	end
endmodule

//16-bit wide mux with 8 inputs
module mux168(
	input [16*8-1:0] I,
	input [2:0] sel,
	output reg [15:0] Y
);
	always @(I,sel)
	begin
		case(sel)
			0: Y = I[0+:16];
			1: Y = I[16+:16];
			2: Y = I[32+:16];
			3: Y = I[48+:16];
			4: Y = I[64+:16];
			5: Y = I[80+:16];
			6: Y = I[96+:16];
			7: Y = I[112+:16];
			default: Y=16'dz;
		endcase
	end

endmodule
